// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TwinerSticker.h"
#include "TwinerStickerGameMode.h"
#include "TwinerStickerPawn.h"

ATwinerStickerGameMode::ATwinerStickerGameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = ATwinerStickerPawn::StaticClass();
}

